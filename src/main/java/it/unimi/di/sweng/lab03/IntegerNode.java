package it.unimi.di.sweng.lab03;

public class IntegerNode {
	private int i;
	private IntegerNode next = null;
	
	public IntegerNode(int i){
		this.i=i;
	}
	public String toString(){
		if (next==null) return i+"";
		else return i+ " " + next.toString();
	}
	public void addLast(int i){
		if(next==null) next = new IntegerNode(i);
		else next.addLast(i);
	}
}
