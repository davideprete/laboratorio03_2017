package it.unimi.di.sweng.lab03;

public class IntegerList {

private IntegerNode head=null;
	
	public IntegerList(String string) {
		if(!string.matches("(\\d+ ?)*")) throw new IllegalArgumentException("NaN");
		for(String s:string.split(" "))
			if (!s.equals("")) addLast(Integer.valueOf(s));
			
	}
	public IntegerList() {
	}
	public String toString(){
		if(head==null) return "[]";
		else return "["+ head.toString() +"]";
	}
	public void addLast(int i) {
		if(head==null) head=new IntegerNode(i);
		else head.addLast(i);
	}
}
